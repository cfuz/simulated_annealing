#!/usr/bin/python3.7
# coding: utf-8

class Logger:
    def __init__(self, mode=False):
        self.explicit = mode

    def log_network(self, graph):
        if self.explicit:
            print("➜  Graph:")
            print("   ⚙  Type: Undirected")
            print("   ⚙  Number of nodes: {}".format(graph.number_of_nodes()))
            print("   ⚙  Number of edges: {}".format(graph.number_of_edges()))
            print("   ⚙  Nodes:")
            for node in graph.nodes():
                print("      ⇢  Node#{:<5} ⌅  Cluster: {:<5}".format(
                    node,
                    graph.nodes[node]['cluster'] if 'cluster' in graph.nodes[node] else "None"
                ))
            print("   ⚙  Edges:")
            for source, destination, data in graph.edges.data():
                print("      ⇢  {:<30} [ aff.: {:<+10.3f} ]".format(
                    "Node#{} ⇌  Node#{}".format(
                        source,
                        destination
                    ),
                    data['affinity']
                ))
    
    def log_candidate(self, partition, score = None):
        if self.explicit:
            print("➜  Candidate:")
            print("   ⚙  {:<19} {}".format(
                "Number of clusters:",
                max(partition)
            ))
            print("   ⚙  {:<19} {}".format(
                "Partition:",
                partition
            ))
            if score:
                print("   ⚙  {:<19} {:.3f}".format(
                    "Score:",
                    float(score)
                ))

    def log_partitions(self, partitions, scores = None, candidates = None):
        if candidates is None:
            candidates = []
        if scores is None:
            scores = []
        if self.explicit:
            print("➜  Partitions:")
            for partition_index, partition in enumerate(partitions):
                print("   ⚙  {:<19} {}".format(
                    "Number of clusters:",
                    max(partition)
                ))
                print("   ⚙  {:<19} {}".format(
                    "Partition:",
                    partition
                ))
                if scores:
                    print("   ⚙  {:<19} {:.3f}".format(
                        "Score:",
                        scores[partition_index][0]
                    ))
                    print("   ⚙  {:<19} {}".format(
                        "Defects:",
                        scores[partition_index][1]
                    ))
                if candidates:
                    print("   ⚙  {:<19} {}".format(
                        "Candidates:",
                        candidates[partition_index]
                    ))

    def info(self, message):
        if self.explicit:
            print("➜  {}".format(message))

    def success(self, message):
        if self.explicit:
            print("✔  {}".format(message))

    def update(self, message):
        if self.explicit:
            print("ϟ  {}".format(message))

    def warning(self, message):
        if self.explicit:
            print("☛  {}".format(message))

    def error(self, message):
        if self.explicit:
            print("✘  {}".format(message))
