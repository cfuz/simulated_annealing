#!/usr/bin/python3.7
# coding: utf-8
import plotly.graph_objects as go
import plotly.express as px

from plotly.subplots import make_subplots



class Drawer:
    def __init__(self, logger, max_affinity, min_affinity, n_ranges):
        self.logger = logger
        self.affinity_ranges = []
        range_width = float((max_affinity - min_affinity) / n_ranges)
        for range_index in range(n_ranges):
            self.affinity_ranges.append((
                min_affinity + (range_index * range_width),
                min_affinity + ((range_index + 1) * range_width) if range_index < n_ranges - 1 else min_affinity + ((range_index + 1) * range_width) + 0.1
            ))
        self.plotter = Plotter(logger, self.affinity_ranges)

    ###
    # Trace l'instance du graphe en 2 et 3 dimensions avec l'évolution du score 
    # et de l'optimal de score fournis en paramètres.
    # @param graph              Instance du graphe annotée avec la meilleure 
    #                           partition en sortie d'heuristique
    # @param dataset_name       Nom du jeu de données analysé
    # @param n_clusters         Nombre de clusters composant la partition finale
    # @param score_dataset      Dictionnaire contenant l'évolution du score au 
    #                           fil des itérations
    # @param best_score_dataset Dictionnaire contenant l'évolution du meilleur 
    #                           score en sortie du recuit simulé
    # @param output_filename    Fichier html de sortie à générer
    ###
    def render(
            self,
            graph,
            dataset_name,
            score,
            n_clusters,
            score_dataset,
            best_score_dataset,
            output_filename
    ):
        self.logger.info("Rendering network for dataset {}..".format(dataset_name))
        # On commence par générer les instances graphiques 3D puis 2D à partir  
        # du réseau fourni en paramètres
        net_plots = [
            self.plotter.plot_network(graph), 
            self.plotter.plot_network(graph, False)
        ]
        # On génère les instances graphiques liées à l'évolution du score
        score_plots = self.plotter.plot_scores(
            score_dataset, 
            best_score_dataset
        )
        # On configure le rendu
        n_rows, n_cols = 2, 2
        fig = make_subplots(
            rows = n_rows,
            cols = n_cols,
            specs = [
                [{"type": "scene"}, {"type": "scatter"}],
                [{"type": "scatter"}, {"type": "scatter"}]
            ],
            subplot_titles = [
                "<i>{}@{:01.3f}: 3D optimal affinity net. clustering <br>#clusters: {}</i>".format(
                    dataset_name,
                    score,
                    n_clusters
                ),
                "<i>Score evolution through successive iterations</i>",
                "<i>2D associated perspective</i>",
                "<i>Optimal score evolution according to iterations</i>"
            ]
        )
        for plot_id in range(1, 3):
            for edge_plot in net_plots[0][0]:
                fig.add_trace(edge_plot, row = plot_id, col = 1)
            fig.add_trace(net_plots[0][1], row = plot_id, col = 1)
            del net_plots[0]
        for plot_id, plots in enumerate(score_plots, 1):
            for plot in plots:
                fig.add_trace(plot, row = plot_id, col = 2)
        fig.update_layout({
            'template': "plotly_white",
            'title_text': "<b>Optimal network clustering representations given the mode and modularity evolution</b>",
            'margin': {'t': 100},
            'hovermode': 'closest',
            'showlegend': True,
        })
        fig.update_xaxes(title_text = "Iteration", row = 1, col = 2)
        fig.update_yaxes(title_text = "Score", row = 1, col = 2)
        fig.update_xaxes(title_text = "Iteration", row = 2, col = 2)
        fig.update_yaxes(title_text = "Score", row = 2, col = 2)
        fig.write_html(output_filename)


class Plotter:
    def __init__(self, logger, affinity_ranges):
        self.logger = logger
        self.affinity_ranges = affinity_ranges
        self.color_scale = px.colors.sequential.Viridis
        self.tracer = Tracer(logger)

    ###
    # Génère l'instance graphique en charge de représenter les données d'un 
    # graphe
    # @param graph  Instance du graphe annoté
    # @param tridim Booléen déterminant si le tracé est en 3D ou en 2D
    # @return       Instances graphiques des arcs et noeuds
    ###
    def plot_network(self, graph, tridim = True):
        self.logger.info("Plotting {}D graph..".format(3 if tridim else 2))
        return (self.plot_edges(graph, tridim), self.plot_nodes(graph, tridim))

    ###
    # Génère l'instance graphique en charge de représenter l'évolution des 
    # scores.
    # @param score_dataset      Hashmap contenant l'évolution des scores
    # @param best_score_dataset Hashmap contenant l'évolution du meilleur score
    # @return                   Instances graphiques des évolutions des 
    #                           différents type de score
    ###
    def plot_scores(self, score_dataset, best_score_dataset):
        self.logger.info("Plotting scores..")
        return (
            self.plot_score(score_dataset, "Score evo."), 
            self.plot_score(best_score_dataset, "Opt. evo.")
        )

    ###
    # Génère l'instance graphique des arcs composant un graphe
    # @param graph  Instance du graphe annoté
    # @param tridim Booléen déterminant si le tracé est en 3D ou en 2D
    # @return       Instances graphiques des arcs catégorisés / colorisés en 
    #               fonction de la tranche de valeur d'affinité.
    ###
    def plot_edges(self, graph, tridim = True):
        edge_plots = []
        for range_index, affinity_range in enumerate(self.affinity_ranges):
            trace = self.tracer.edge_trace(
                graph, 
                affinity_range, 
                3 if tridim else 2
            )
            config = {
                'x': trace[0],
                'y': trace[1],
                'mode': "lines",
                'opacity': 0.5,
                'line': {
                    'color': self.color_scale[2 * range_index],
                    'width': 0.75,
                },
                'hoverinfo': "none",
                'name': "{}D edges (aff.: [{},{}[)".format(
                    3 if tridim else 2,
                    affinity_range[0],
                    affinity_range[1]
                )
            }
            if tridim:
                config['z'] = trace[2]
                edge_plots.append(go.Scatter3d(config))
            else:
                edge_plots.append(go.Scatter(config))
        return edge_plots

    ###
    # Génère l'instance graphique des noeuds composant un graphe
    # @param graph  Instance du graphe annoté
    # @param tridim Booléen déterminant si le tracé est en 3D ou en 2D
    # @return       Instance graphique représentant les noeuds annotés du graphe
    ###
    def plot_nodes(self, graph, tridim = True):
        trace, labels, clusters = self.tracer.node_trace(
            graph,
            3 if tridim else 2
        )
        config = {
            'x': trace[0],
            'y': trace[1],
            'mode': "markers",
            'marker': {
                'showscale': False,
                'symbol': "circle",
                'size': 5 if tridim else 10,
                'color': clusters,
                'colorscale': "Viridis",
                'opacity': 0.75 if tridim else 1.0,
                'colorbar': {
                    'thickness': 5,
                    'title': "Cluster index",
                    'xanchor': "left",
                    'titleside': "right"
                },
                'line': {
                    'color': "rgb(88,110,117)",
                    'width': 0.5,
                },
            },
            'hovertemplate': "%{text}",
            'text': labels,
            'name': "{}D nodes repr.".format(3 if tridim else 2),
            'showlegend': False,
        }
        if tridim:
            config['z'] = trace[2]
            return go.Scatter3d(config)
        else:
            return go.Scatter(config)

    ###
    # Génère l'instance graphique d'un sous jeu de données renseignant 
    # l'évolution d'un score
    # @param score_dataset  Jeu de données à partir duquel générer le tracé
    # @param name           Nom de la courbe
    # @return               Instance graphique représentant l'évolution du 
    #                       score fournit dans le jeu de données
    ###
    def plot_score(self, score_dataset, name):
        score_plots = []
        for setup in score_dataset:
            trace = self.tracer.score_trace(score_dataset[setup])
            config = {
                'x': trace[0],
                'y': trace[1],
                'mode': "lines+markers",
                'name': "{} ({})".format(
                    name,
                    setup
                ),
                'hovertemplate': "%{text}",
                'text': trace[2]
            }
            score_plots.append(go.Scatter(config))
        return score_plots



class Tracer:
    def __init__(self, logger):
        self.logger = logger

    ###
    # Génère la trace des arcs, dont l'affinité est comprise dans l'intervalle 
    # représenté par affinity_range, à partir d'une instance de graphe dont les 
    # noeuds sont annotés.
    # @param graph          Instance d'un graphe annoté
    # @param affinity_range Intervalle d'affinité pour le filtrage des arcs
    # @param dim            Booléen définissant si la trace doit être réalisée 
    #                       en 3D ou non   
    # @return               Trace des arcs dont l'affinité est dans 
    #                       l'intervalle affinity_range
    ###
    def edge_trace(self, graph, affinity_range, dim = 3):
        trace = [[] for _ in range(dim)]
        dim_labels = ['x', 'y', 'z']
        dim_index = 0
        for source, destination, data in graph.edges.data():
            if data['affinity'] < affinity_range[1] and data['affinity'] >= affinity_range[0]:
                while dim_index < dim:
                    trace[dim_index] += [
                        graph.nodes[source][dim_labels[dim_index]],
                        graph.nodes[destination][dim_labels[dim_index]],
                        None
                    ]
                    dim_index += 1
                dim_index = 0
        return trace

    ###
    # Génère la trace des noeuds à partir d'une instance de graphe via leurs 
    # annotations respectives.
    # @param graph          Instance d'un graphe annoté
    # @param dim            Booléen définissant si la trace doit être réalisée 
    #                       en 3D ou non   
    # @return               Trace des noeuds
    ###
    def node_trace(self, graph, dim = 3):
        trace = [[] for _ in range(dim)]
        labels, clusters = [], []
        dim_labels = ['x', 'y', 'z']
        dim_index = 0
        for node, data in graph.nodes.data():
            while dim_index < dim:
                trace[dim_index].append(
                    data[dim_labels[dim_index]]
                )
                dim_index += 1
            dim_index = 0
            labels.append("Node#{}{}".format(
                node,
                "<extra>Cluster#{}</extra>".format(
                    graph.nodes[node]['cluster']
                ) if 'cluster' in graph.nodes[node] else ""
            ))
            clusters.append(
                graph.nodes[node]['cluster'] if 'cluster' in graph.nodes[node] else node
            )
        return trace, labels, clusters

    ###
    # Génère la trace de l'évolution du score à partir d'un jeu de données de 
    # la forme : (iteration, score) 
    # @param score_subdataset   Instance d'un graphe annoté
    # @return                   Trace de l'évolution du score en fonction des 
    #                           itérations
    ###
    def score_trace(self, score_subdataset):
        iterations, scores, labels = [], [], []
        for iteration, score in sorted(
            score_subdataset.items(), 
            key = lambda entry: entry[0]
        ):
            iterations.append(iteration)
            scores.append(score)
            labels.append("Score:{:.5f}{}".format(
                score,
                "<extra>Iteration#{}</extra>".format(iteration)
            ))
        return (iterations, scores, labels)
