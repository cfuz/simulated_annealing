## Community detection engine based on simulated annealing heuristics
This project is about detecting and splitting an affinity network in an automated fashion, by using a simulated annealing heuristics.


## Technologies and frameworks used
All the scripts in the ``src/`` folder are written in <i>Python 3.7.3 (release: Oct. 7 2019)</i>.
The libraries used for this project are as follows:
- [NetworkX 2.4](https://networkx.github.io/documentation/stable/) for network tools
- [Plot.ly 4.4.1](https://plot.ly/python/) for data rendering
- [Pandas 0.25.3](https://pandas.pydata.org/) for colorizing graph's edges given the affinity
- [Numpy 1.18.0](https://numpy.org/doc/)
- [Scipy 1.4.1](https://pypi.org/project/scipy/)

You can install all those libraries throught python's package manager ``pip``, with the ``requirements.txt``  file available at the project's root folder:

```
pip install -r requirements.txt
```

This will setup the project's environment.


## How to use ?
To run the software you will have to put all your input data in the same directory. Let's say ``sample/my_input/``. 
This folder should at least contain a file ``my_input.dat`` providing the list of affinities (i.e. edges with weight between -1 to +1) between the different nodes of the network.
Note that this file must mention the number of nodes contained in the graph.
If you want, you can also put in this same folder a ``community.dat`` file, storing the expected partitioning from the dataset. 
There are already some dataset at your disposal in the ``sample/`` directory.
To run the software, at the root folder of this project, type the following in your favorite command line:

```
python3.7 src/community_detection.py <input_file> <output_folder> [-T0 <initial_temp>] [-tau <cooling_rate>] [-maxc <n_candidates>] [-epsilon <threshold>] [-e | --explicit]
```

Where:
- ``<input_file>`` is the path to your data file
- ``<output_folder>`` is the folder to put outputs in

Optional arguments:
- ``-e`` or ``--explicit`` are options to display the work in progress results
- ``-T0 <initial_temp>`` defines the initial temperature at which the system should start (default: 1000)
- ``-tau <cooling_rate>`` defines the cooling rate of the simulated annealing heuristic (default: 0.5)
- ``-maxc <n_candidates>`` defines the size the size of the pool candidates (default: 10)
- ``-epsilon <threshold>`` defines the threshold at which the heuristic should stop (default: 0.01)

For instance with the preconfigured dataset you could type the following from the root folder's project:

```
python3.7 src/community_detection.py sample/section63/section63.dat out/ -e -T0 100 -maxc 10 -tau 0.001 -epsilon 0.01
```

In this dataset of 200 nodes, the network will be splitted optimally by using the simulated annealing heuristics.
The output will be saved in ``out/test_opt_setup.html``. 
You will then be able to view and interact with the clustering results on a web browser.
