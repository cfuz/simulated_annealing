#!/usr/bin/python3.7
# coding: utf-8
import networkx as nx
import numpy as np
import time

from drawer import Drawer
from logger import Logger
from parser import (
    parse_arguments, 
    parse_graph, 
    parse_clusters,
    parse_scores
)
from siman import SimulatedAnnealing



###
# Pour la représentation graphique en sortie, on détermine les coordonnées des 
# noeuds dans l'espace propre lié au laplacien du graphe passé en paramètre.
# @param graph  Instance du graphe à annoter
###
def annotate_nodes_from_spring_layout(graph):
    # Récupère la position de chaque noeuds du graphe, utilisant l'algorithme 
    # de Fruchterman-Reingold
    spring_layout = nx.spring_layout(
        graph,
        dim = 3,
        weight = 'affinity'
    )
    # On récupère les coordonnées de chaque noeuds et on annote le graphe en 
    # conséquence
    for key in spring_layout:
        coordinates = np.array(spring_layout[key])
        graph.nodes[key]['x'], graph.nodes[key]['y'], graph.nodes[key]['z'] = coordinates

###
# Annote les noeuds d'une instance de graphe, à partir d'une partition fournie 
# sous forme de liste.
# @param graph      Instance du graphe à annoter
# @param partition  Liste des clusters telle que partition[noeud] = cluster
###
def clusterize(graph, partition):
    for node, cluster in enumerate(partition):
        graph.nodes[node + 1]['cluster'] = cluster

###
# Traite l'évolution des scores issus du parsing d'un des fichiers de score 
# générés par l'heuristique. Ne traite que les données correspondant au jeu de 
# données sample_name.
# N.B.:
# La façon dont sont construits les fichiers de score dans le dossier out/data/ 
# fait que ce calcul de moyenne ne sert au final à rien étant donné que l'on 
# écrase les fichiers existants à chaque nouvelle itération.
# @param score_dataset  Jeu de données contenant l'évolution des scores en 
#                       fonction des métaparamètres d'entrée
# @param sample_name    Nom du jeu de données (nom de base du fichier d'entrée)
# @return               Hashmap contenant pour chaque metaparamètres, la 
#                       moyenne des scores pour chaque itérations.
###
def compute_avg_score_iteration_fixed(score_dataset, sample_name):
    avg_score_it_fixed= {}
    for setup in score_dataset[sample_name]:
        if setup not in avg_score_it_fixed:
            avg_score_it_fixed[setup] = {}
        for iteration in score_dataset[sample_name][setup]:
            avg = 0.0
            for score in score_dataset[sample_name][setup][iteration]:
                avg += score
            avg /= len(score_dataset[sample_name][setup][iteration])
            avg_score_it_fixed[setup][iteration] = avg
    return avg_score_it_fixed

def register_results(
    input_filename, 
    partition_filename, 
    heuristic, 
    resolution_time
):
    # On inscrit toutes les données relative à la partition dans un fichier
    file = open(partition_filename, "w")
    file.write("Input file: {}\n".format(input_filename))
    file.write("T0:{} tau:{:.3f} maxc:{} epsilon:{:.3f}\n".format(
        heuristic.initial_temperature,
        heuristic.cooling_rate,
        heuristic.max_candidates,
        heuristic.epsilon
    ))
    file.write("Score: {}\n".format(
        heuristic.best_setup['score']
    ))
    file.write("Time: {}s\n".format(
        resolution_time
    ))
    file.write("Partition (node - cluster):\n")
    for node, cluster in enumerate(heuristic.best_setup['partition'], 1):
        file.write("{}\t{}\n".format(
            node,
            cluster
        ))
    file.close()



if __name__ == "__main__":
    metaparameters = parse_arguments()
    logger = Logger(metaparameters['explicit'])
    # On construit le graphe a partir du fichier fournit en argument a l'appel 
    # du script
    graph, max_affinity, min_affinity = parse_graph(metaparameters['input'])
    annotate_nodes_from_spring_layout(graph)
    # On vérifie et met à jour les attributs des noeuds si une réalité de 
    # terrain existe
    metaparameters['expected'] = parse_clusters(
        graph,
        metaparameters['communities']
    )
    if metaparameters['expected']:
        parse_clusters(graph, metaparameters['communities'])
        logger.success("Cluster data base has been detected. Updating graph..")
    else:
        logger.warning("No cluster data base. Will not be able to display expected splitting results")
    # logger.log_network(graph)
    drawer = Drawer(logger, max_affinity, min_affinity, 4)
    # On instancie l'heuristique et on définit ses métaparamètres si 
    # l'utilisateur en a renseigné au lancement de l'application
    heuristic = SimulatedAnnealing(logger, graph)
    if 'maxc' in metaparameters:
        heuristic.max_candidates = metaparameters['maxc']
    if 'T0' in metaparameters:
        heuristic.initial_temperature = metaparameters['T0']
    if 'tau' in metaparameters:
        heuristic.cooling_rate = metaparameters['tau']
    if 'epsilon' in metaparameters:
        heuristic.epsilon = metaparameters['epsilon']
    logger.info("Pool candidates:")
    logger.log_partitions(heuristic.candidates)
    t_start = time.process_time()
    heuristic.fire(
        metaparameters['input_basename'],
        metaparameters['best_scores_file'],
        metaparameters['scores_file']
    )
    t_stop = time.process_time()
    clusterize(graph, heuristic.best_setup['partition'])
    # On récupère l'évolution des score pour le jeu de données fournit au 
    # lancement du script
    avg_score_evo_it_fixed = compute_avg_score_iteration_fixed(
        parse_scores(metaparameters['scores_file']),
        metaparameters['input_basename']
    )
    avg_best_score_evo_it_fixed = compute_avg_score_iteration_fixed(
        parse_scores(metaparameters['best_scores_file']),
        metaparameters['input_basename']
    )
    # On inscrit toutes les données relative à la partition dans un fichier
    logger.info("Registering best setup in {} file..".format(
        metaparameters['partition_file']
    ))
    register_results(
        metaparameters['input'], 
        metaparameters['partition_file'], 
        heuristic,
        t_stop - t_start
    )
    drawer.render(
        graph,
        metaparameters['input_basename'],
        heuristic.best_setup['score'],
        max(heuristic.best_setup['partition']),
        avg_score_evo_it_fixed,
        avg_best_score_evo_it_fixed,
        metaparameters['output_html']
    )
    logger.warning("Time spend: {:.3f}s".format(
        t_stop - t_start
    ))
    logger.success("Done!")
