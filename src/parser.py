#!/usr/bin/python3.7
# coding: utf-8
import os
import sys
import math
import networkx as nx
import numpy as np

# Meta-parameters
expected_n_args = 2
optional_n_args = 9
optional_index = expected_n_args + optional_n_args


###
# Traite les arguments renseignés par l'utilisateur et génère les 
# metaparamètres (nom du fichier de sortie, nom de base du fichier d'entrée, 
# mode explicite de fonctionnement, etc.) du programme.
###
def parse_arguments():
    check_number_of_arguments()
    parsed_arguments = {}
    parsed_arguments = setup_output_file_system()
    parsed_arguments.update(check_optionals())
    return parsed_arguments

def show_input_advice():
        print("✘  Input command from project's root directory should be of type:")
        print("   python src/community_detection.py input_file output_directory [-T0 <initial_temp>] [-tau <cooling_rate>] [-epsilon <threshold>] [-maxc <n_candidates>] [-e | --explicit]")
        print("☛  For more information check the readme.md file..")

###
# Vérifie le nombre d'arguments entrés lors du lancement du script 
# community_detection.py
# Sort du système si les paramètres d'entrée ne sont pas bien renseignés
# par l'utilisateur.
###
def check_number_of_arguments():
    if len(sys.argv) < expected_n_args + 1:
        print("✘  Wrong number of arguments given. Expected at least {}, gave {}".format(
            expected_n_args,
            len(sys.argv) - 1
        ))
        show_input_advice()
        sys.exit()
    elif len(sys.argv) > expected_n_args + optional_n_args + 1:
        print("✘  Too many arguments given. Expected at most {}, gave {}".format(
            optional_index,
            len(sys.argv) - 1
        ))
        show_input_advice()
        sys.exit()
    elif len(sys.argv) > expected_n_args + 1:
        if "-e" in sys.argv or "--explicit" in sys.argv:
            if len(sys.argv) % 2 != 0:
                print("✘  Wrong number of arguments given. Expected odd number of arguments.")
                show_input_advice()
                sys.exit()
        else:
            if len(sys.argv) % 2 != 1:
                print("✘  Wrong number of arguments given. Expected even number of arguments.")
                show_input_advice()
                sys.exit()


###
# Traite le nom et chemin du fichier d'entrée pour paramétrer le nom du fichier 
# en sortie avec les dossiers qui conviennent.
###
def setup_output_file_system():
    # Extraction du nom du fichier d'entree sans son extension
    input_basename = os.path.basename(sys.argv[1])
    input_directory = os.path.dirname(sys.argv[1])
    input_basename = os.path.splitext(input_basename)[0]
    input_community = "{}/{}".format(
        input_directory,
        "community.dat"
    )
    # Nom du fichier de sortie
    output_filename = sys.argv[2] + input_basename + "_opt_setup.html"
    # Nom du fichier contenant les donnees de modularite
    data_directory = sys.argv[2] + "data/"
    try:
        os.makedirs(data_directory)
    except FileExistsError:
        print("✔  Directory ", data_directory, " already exists. It will be used for output .dat files storing..")
    data_filename = data_directory + input_basename + "_output.dat"
    return {
        'input': sys.argv[1],
        'input_basename': input_basename,
        'input_dir': input_directory,
        'communities': input_community,
        'output_html': output_filename,
        'output_data': data_filename,
        'best_scores_file': "{}{}_best_scores.dat".format(
            data_directory, 
            input_basename
        ),
        'scores_file': "{}{}_scores.dat".format(
            data_directory, 
            input_basename
        ),
        'partition_file': "{}{}_partition.dat".format(
            data_directory, 
            input_basename
        ),
        'data_dir': data_directory,
        'expected': False
    }


###
# Vérifie la présence du paramètre optionnel de l'application
# NOTA:
# 	Le mode --explicit ou -e affichera sur la console la progression de 
# 	l'algorithme, impactant par conséquent le temps de résolution de celui-ci.
# 	Mode à proscrire si vous souhaitez être dans des conditions de performances 
# 	optimales..
###
def check_optionals():
    options = { 'explicit': False }
    n_args = len(sys.argv)
    if "-e" in sys.argv or "--explicit" in sys.argv:
        options['explicit'] = True
    if "-T0" in sys.argv:
        options['T0'] = int(sys.argv[
            sys.argv.index("-T0") + 1
        ])
    if "-tau" in sys.argv:
        options['tau'] = float(sys.argv[
            sys.argv.index("-tau") + 1
        ])
    if "-maxc" in sys.argv:
        options['maxc'] = int(sys.argv[
            sys.argv.index("-maxc") + 1
        ])
    if "-epsilon" in sys.argv:
        options['epsilon'] = float(sys.argv[
            sys.argv.index("-epsilon") + 1
        ])
    return options


###
# Construit un graphe à partir d'une liste d'arcs renseignés dans un fichier
# 	--> Lien vers la librairie networkx pour la construction de graphes : 
# 	https://networkx.github.io/documentation/stable/tutorial.html
###
def parse_graph(edges_filename):
    graph = nx.Graph()
    edges = []
    max_affinity, min_affinity = -math.inf, +math.inf
    # Ouverture et lecture du fichier contenant les informations du graphe
    # n.b. 
    #   On supposera que les entrees du fichiers prennent la forme 
    #       "source\tdestination\n"
    #   On supposera egalement que le graphe demande est un graphe non-oriente
    file = open(edges_filename, "r")
    n_instances = int(file.readline().split("\n")[0].split()[0])
    graph.add_nodes_from(np.arange(1, n_instances))
    # Pour savoir si l'ID du premier noeud est un 0 ou un 1
    doomed_input_file = False
    for line in file:
        buffer = line.split("\n")[0].split()
        source, destination = int(buffer[0]), int(buffer[1])
        if source == 0 or destination == 0:
            doomed_input_file = True
        edges.append((
            # Source
            source if not doomed_input_file else source + 1,
            # Destination
            destination if not doomed_input_file else destination + 1,
            # Poids
            {
                'affinity': float(buffer[2]),  # Affinité Source - Destination
                'visited': False  # Pour le calcul de score
            }
        ))
        if float(buffer[2]) < min_affinity:
            min_affinity = float(buffer[2])
        if float(buffer[2]) > max_affinity:
            max_affinity = float(buffer[2])
    graph.add_edges_from(edges)
    file.close()
    return graph, max_affinity, min_affinity

###
# Attribue les groupes d'appartenance aux noeuds d'un graphe à partir d'une 
# réalité de terrain, renseignée dans le fichier clusters_filename.
###
def parse_clusters(graph, clusters_filename):
    try:
        file = open(clusters_filename, "r")
        for line in file:
            buffer = line.split("\n")[0].split()
            graph.nodes[int(buffer[0])]['cluster'] = int(buffer[1])
            file.close()
            return True
    except IOError:
        return False


###
# Récupère les données générées lors des itérations successives du simulé 
# recuit.
# @param benchmark_filename Nom du fichier à parser
# @return                   Dataset retourné est une hashmap ayant la structure:
#                           { setup: { k: [ score ] } }
#                           Où:
# 	                        --	setup   est une string renseignant sur les 
#                                       metaparamètres du jeu de données
#                           --	k		est le pas d'itération
#                           --	score 	est l'évaluation de la qualité de la 
#                                       partition obtenue à l'itération k
# 				                        de l'algorithme
###
def parse_scores(scores_filename):
    dataset = {}
    file = open(scores_filename, 'r')
    for entry in file:
        line = entry.split("\n")[0].split("\t")
        sample_name, metaparameters, iteration, score = line[0], line[1], int(line[2]), float(line[3])
        if sample_name not in dataset:
            dataset[sample_name] = {}
        if metaparameters not in dataset[sample_name]:
            dataset[sample_name][metaparameters] = { iteration: [ score ] }
        else:
            if iteration not in dataset[sample_name][metaparameters]:
                dataset[sample_name][metaparameters][iteration] = [ score ]
            else:
                dataset[sample_name][metaparameters][iteration].append(score)
    file.close()
    return dataset
