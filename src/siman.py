#!/usr/bin/python3.7
# coding: utf-8
import networkx as nx
import numpy as np
import math
import random



class SimulatedAnnealing:
    def __init__(
        self,
        logger,
        graph
    ):
        logger.info("Init. heuristic parameters..")
        self.logger = logger
        self.graph = graph
        self.max_candidates = 10
        self.initial_temperature = 1000
        self.cooling_rate = .01
        self.candidates = self.init_random_setup(graph.number_of_nodes())
        # self.candidates = self.init_positive_based_links(graph)
        self.best_setup = {'score': math.inf, 'partition': []}
        self.epsilon = 0.01

    ###
    # Création d'un jeu de partitions définies aléatoirement
    # @param n_nodes    Nombre de noeuds composants le graphe. Limite le nombre
    #                   maximum de clusters
    # @return           Liste de taille self.max_candidates de candidats
    ###
    def init_random_setup(self, n_nodes):
        # Définition du dictionnaire des partitions
        candidates = []
        for _ in range(self.max_candidates):
            partition = []
            n_clusters = random.randint(2, n_nodes)
            for _ in range(n_nodes):
                partition.append(random.randint(1, n_clusters))
            if max(partition) > 1:
                candidates.append(partition)
        return candidates

    ###
    # Création d'un jeu de partitions basée sur la construction par liens 
    # d'affinités positifs entre les individus d'une instance de graphe.
    # N.B.:
    # Cet algorithme est un ajustement de l'algorithme de génération de 
    # candidats initiaux basé sur le travail de J. DURET.
    # @param graph  Instance du graphe sur laquelle générer le pool de 
    #               partitions
    # @return       Liste de candidats initiaux pour le commencement de 
    #               l'heuristique
    ###
    def init_positive_based_links(self, graph):
        candidates = []
        for _ in range(self.max_candidates):
            nodes = np.asarray(graph.nodes())
            partition = np.empty(nodes.shape, dtype = np.int32)
            idx = 1
            while nodes.shape[0]:
                nodes = self.random_positive_path(
                    graph, 
                    nodes, 
                    partition, 
                    idx
                )
                idx += 1
            candidates.append(partition.tolist())
        return candidates

    def random_positive_path(self, graph, nodes, part, idx):
        node = np.random.choice(nodes)
        nodes = np.delete(nodes, np.where(nodes == node))
        part[node-1] = idx
        while True:
            neighbors = [edge[1] for edge in graph.edges(node) if graph[edge[0]][edge[1]]['affinity'] > 0 and edge[1] in nodes]
            if len(neighbors) == 0:
                return nodes
            next_node = np.random.choice(neighbors)
            nodes = np.delete(nodes, np.where(nodes == next_node))
            node = next_node
            part[node-1] = idx

    ###
    # Réatribue les identifiants de clusters par mapping des différentes
    # valeurs composant la partition.
    # @param partition  Partition à remapper
    # @return           Partition remappée sans cluster vide
    ###
    def remap_candidate(self, candidate):
        cluster_map = {}
        cluster_counter = 1
        remapped_candidate = []
        for cluster in candidate:
            if cluster not in cluster_map:
                cluster_map[cluster] = cluster_counter
                remapped_candidate.append(cluster_counter)
                cluster_counter += 1
            else:
                remapped_candidate.append(cluster_map[cluster])
        return remapped_candidate

    ###
    # Génère un voisin d'un candidat en permutant un noeud aléatoirement choisi
    # dans un cluster définit aléatoirement
    # @param candidate  Partition à partir de laquelle générer le voisinage
    # @return           Une solution voisine du candidat
    ###
    def neighbor_from_random_swap(self, candidate):
        n_nodes = self.graph.number_of_nodes()
        index = random.randint(0, n_nodes - 1)
        neighbor = candidate.copy()
        rand = random.randint(1, n_nodes)
        while neighbor[index] == rand :
            rand = random.randint(1, n_nodes)
        neighbor[index] = rand
        return neighbor
    
    ###
    # [liaison anormale interne] 
    # Réatribution aléatoire d'un noeud faisant partie d'une liaison 
    # intra-cluster défaillante.
    # @param edge       Arc défectueux sur lequel baser le nouveau voisinage
    # @param candidate  Candidat soumis à mutation
    # @return           Voisin du candidat fournit en paramètre
    ###
    def neighbor_from_split(self, edge, candidate):
        # On prend au hasard l'une des deux extrémités composant l'arc 
        # défectueux
        node = edge[0] if random.random() < 0.5 else edge[1]
        # On copie la liste parente pour en générer un voisin
        neighbor = list(candidate)
        # Une chance sur deux de générer une nouvelle partition lors de la 
        # division
        n_clusters = max(candidate)
        if n_clusters < len(candidate) and random.random() < 0.5:
            n_clusters = n_clusters + 1
        # On attribue un nouveau cluster au noeud node
        while True:
            new_cluster = random.randint(1, n_clusters)
            if new_cluster != candidate[node - 1]:
                neighbor[node - 1] = new_cluster
                break
        return neighbor

    ###
    # [liaison anormale externe] 
    # Modifie le cluster auquel appartient un des noeuds d'un arc inter-cluster 
    # défectueux, au cluster de l'autre noeud composant la liaison défaillante.
    # @param edge       Arc défectueux sur lequel baser le nouveau voisinage
    # @param candidate  Candidat soumis à mutation
    # @return           Voisin du candidat fournit en paramètre
    ###
    def neighbor_from_merge(self, edge, candidate):
        from_node, to_node = edge if random.random() < 0.5 else reversed(edge)
        neighbor = list(candidate)
        neighbor[from_node - 1] = candidate[to_node - 1]
        return neighbor

    ###
    # Génère un voisin d'un candidat en permutant un noeud d'une de ses  
    # liaisons défectueuses, en fonction du type de cette dernière.
    # @param candidate  Partition à partir de laquelle générer le voisinage
    # @param defects    Liste des arcs augmentant la valeur du score du candidat
    # @return           Une parititon voisine du candidat
    ###
    def neighbor_from_defective_edges(self, candidate, defects):
        defect = defects[random.randint(
            0, 
            len(defects) - 1
        )]
        source = random.randint(0,1)
        # Si c'est une liaison entre deux clusters qui pose problème
        neighbor = []
        if defect[2] == "outer":
            neighbor = self.neighbor_from_merge(
                (defect[0], defect[1]),
                candidate
            )
        # Si c'est une liaison inter-cluster qui pose problème, il faut 
        # soustraire l'un des deux noeuds composant la liaison pour l'ajouter 
        # dans une autre partition.
        else:
            neighbor = self.neighbor_from_split(
                (defect[0], defect[1]),
                candidate
            )
        return neighbor

    ###
    # Calcule la probabilité d'accepter un nouveau candidat comme solution
    # @param current_energy     Énergie actuelle du système
    # @param candidate_energy   Énergie du candidat
    # @param temperature        Température actuelle du système
    # @return                   La probabilité d'accepter le nouveau candidat
    #                           comme solution
    ###
    def acceptance_probability(
        self,
        current_energy,
        candidate_energy,
        temperature
    ):
        if current_energy > candidate_energy:
            return 1.1
        return math.exp((current_energy - candidate_energy) / temperature)

    ###
    # Évalue la qualité d'une partition en calculant son score et identifiant
    # les arcs anormaux.
    # @param partition  Partition à partir de laquelle calculer le score
    # @return           Un couple comportant le score ainsi que la liste des
    #                   anomalies liées à la partition fournie en paramètres
    ###
    def score(self, partition):
        result = 0
        defects = []
        for source, destination, data in self.graph.edges.data():
            affinity = data['affinity']
            if partition[source-1] == partition[destination-1] and affinity < 0:
                result -= affinity
                defects.append((source, destination, 'inner'))
            if partition[source-1] != partition[destination-1] and affinity > 0:
                result += affinity
                defects.append((source, destination, 'outer'))
        return result, defects

    ###
    # Renseigne un score préformaté dans un fichier de données prévu à cet 
    # effet.
    # @param file           Fichier dans lequel inscrire le score
    # @param sample_name    Nom du jeu de donnée
    # @param n_iterations   Itération à laquelle le score à été trouvé
    # @param score          Valeur de la fonction objectif à n_iterations 
    #                       (inscrit la valeur du meilleur score si non 
    #                       renseigné)
    ###
    def register_score(
        self, 
        file, 
        sample_name, 
        n_iterations, 
        score = None
    ):
        file.write(
            "{}\tT0:{} tau:{:.3f} maxc:{} epsilon:{:.3f}\t{}\t{:.5f}\n".format(
                sample_name,
                self.initial_temperature,
                self.cooling_rate,
                self.max_candidates,
                self.epsilon,
                n_iterations,
                score if score else self.best_setup['score']
            )
        )

    ###
    # Détermine la meilleure configuration (score et partition) à partir de la
    # méthode du simulé recuit.
    # @return   La partition optimale suite au déroulement de l'heuristique
    #           sus-mentionnée.
    ###
    def fire(
        self, 
        input_basename, 
        best_scores_filename, 
        scores_filename
    ):
        self.logger.info("Fire!")
        # Pour écriture de l'avancement de l'optimal de modularité au fil des 
        # itérations
        best_scores_file = open(best_scores_filename, "w")
        scores_file = open(scores_filename, "w")
        temperature = self.initial_temperature
        # On sélectionne une partition dans la piscine des candidats disponibles
        current_candidate = self.candidates[random.randint(
            0,
            len(self.candidates) - 1
        )]
        self.logger.update("Selected candidate for heuristic:")
        self.logger.log_candidate(
            current_candidate,
            self.score(current_candidate)[0]
        )
        self.best_setup['partition'] = current_candidate
        self.best_setup['score'] = self.score(current_candidate)[0]
        current_energy, defects = self.score(current_candidate)
        n_iterations = 0
        self.register_score(
            best_scores_file, 
            input_basename, 
            n_iterations
        )
        self.register_score(
            scores_file,
            input_basename,
            n_iterations
        )
        while temperature > self.epsilon:
            # neighbor = self.neighbor_from_random_swap(current_candidate)
            neighbor = self.neighbor_from_defective_edges(
                current_candidate,
                defects
            )
            neighbor_energy = self.score(neighbor)[0]
            if self.acceptance_probability(
                current_energy,
                neighbor_energy,
                temperature
            ) > random.random():
                current_candidate = neighbor
                current_energy, defects = self.score(current_candidate)
                self.register_score(
                    scores_file,
                    input_basename,
                    n_iterations,
                    current_energy
                )
            # On met à jour la configuration optimale si le candidat actuel
            # affiche un meilleur score que la partition best_candidate
            if self.best_setup['score'] > current_energy:
                self.best_setup['partition'] = current_candidate
                self.best_setup['score'] = current_energy
                self.logger.update("Found new best setup:")
                self.logger.log_candidate(
                    self.best_setup['partition'],
                    self.best_setup['score']
                )
                # On inscrit le meilleur score dans un fichier 
                self.register_score(
                    best_scores_file, 
                    input_basename, 
                    n_iterations
                )
            # On refroidit le système
            temperature *= (1 - self.cooling_rate)
            n_iterations += 1 
        # On recartographie la partition pour purger le candidat de clusters
        # vides
        self.best_setup['partition'] = self.remap_candidate(self.best_setup['partition'])
        self.logger.success("Simulated annealing finished in {} iterations!".format(n_iterations))
        self.logger.log_candidate(
            self.best_setup['partition'],
            self.best_setup['score']
        )
        best_scores_file.close()
        scores_file.close()
